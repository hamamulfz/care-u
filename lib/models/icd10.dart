class Icd10 {
  List<KodeIcd10> kodeIcd10;

  Icd10({this.kodeIcd10});

  Icd10.fromJson(Map<String, dynamic> json) {
    if (json['kode icd 10'] != null) {
      kodeIcd10 = new List<KodeIcd10>();
      json['kode icd 10'].forEach((v) {
        kodeIcd10.add(new KodeIcd10.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.kodeIcd10 != null) {
      data['kode icd 10'] = this.kodeIcd10.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class KodeIcd10 {
  String kodeICD10;
  String diagnosa;
  String diskripsi;

  KodeIcd10({this.kodeICD10, this.diagnosa, this.diskripsi});

  KodeIcd10.fromJson(Map<String, dynamic> json) {
    kodeICD10 = json['Kode ICD-10'];
    diagnosa = json['Diagnosa'];
    diskripsi = json['Diskripsi'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Kode ICD-10'] = this.kodeICD10;
    data['Diagnosa'] = this.diagnosa;
    data['Diskripsi'] = this.diskripsi;
    return data;
  }
}