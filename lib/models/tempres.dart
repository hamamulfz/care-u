class TempRes {
  String date;
  String note;
  String code;
  String name;
  String detail;
  String time;

  TempRes({this.date, this.note, this.name, this.detail, this.time});

  TempRes.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    note = json['note'];
    code = json['code'];
    name = json['name'];
    detail = json['detail'];
    time = json['time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['note'] = this.note;
    data['code'] = this.code;
    data['name'] = this.name;
    data['detail'] = this.detail;
    data['time'] = this.time;
    return data;
  }
}
