import 'package:flutter/material.dart';

class CareUBoxItem extends StatelessWidget {
  CareUBoxItem({
    this.image,
    this.title,
    this.icon,
    this.onTap,
  });
  final String image;
  final String title;
  final IconData icon;
  final Function onTap;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 3),
      child: Material(
        borderRadius: BorderRadius.circular(15),
        elevation: 5,
        child: InkWell(
          onTap: onTap,
          child: Container(
            padding: EdgeInsets.all(3),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(15)),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 40,
                    child: image != null
                        ? Image.asset(
                            image,
                            color: Colors.white,
                          )
                        : Icon(
                            icon,
                            color: Colors.white,
                            size: 35,
                          ),
                  ),
                ),
                SizedBox(width: 10),
                Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    child: Text(
                      title,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
