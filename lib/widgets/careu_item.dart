import 'package:flutter/material.dart';

class CareUItem extends StatelessWidget {
  final String title;
  final String image;
  final IconData icon;
  final Function onTap;
  final double lengthText;
  CareUItem({
    this.image,
    this.title,
    this.onTap,
    this.icon,
    this.lengthText = 0.2,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 10,
        child: Row(
          children: <Widget>[
            Material(
              elevation: 10,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                topLeft: Radius.circular(10),
              ),
              child: InkWell(
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      topLeft: Radius.circular(10),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: 40,
                      child: image != null
                          ? Image.asset(
                              image,
                              color: Colors.white,
                            )
                          : Icon(
                              icon,
                              color: Colors.white,
                            ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Container(
                width: MediaQuery.of(context).size.width * lengthText,
                child: Text(title))
          ],
        ),
      ),
    );
  }
}
