import 'package:careu/screens/auth/login_screen.dart';
import 'package:careu/screens/auth/splash_screen.dart';
import 'package:careu/screens/auth/welcome_screen.dart';
import 'package:careu/screens/main_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

var _defaultHome;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final _prefs = await SharedPreferences.getInstance();
  String user = _prefs.getString('user');
  _defaultHome = user == null ? SplashScreen() : MainScreen();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: _defaultHome,
    );
  }
}
