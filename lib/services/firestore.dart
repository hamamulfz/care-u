import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class FirestoreDb {
  FirestoreDb({
    @required this.uid,
  }) : assert(uid != null);
  String uid;

  Future<void> _setData({path, Map<String, dynamic> data}) async {
    final documentReference = Firestore.instance.document(path);
    await documentReference.setData(data);
  }

  Future<void> _pushData({collectionPath, Map<String, dynamic> data}) async {
    final docReferee = Firestore.instance.collection(collectionPath);
    // print(docReferee.document().documentID);
    // data['fsid'] = docReferee.id;
    await docReferee.add(data);
  }

  void updateData(path, code) {
    try {
      final reference = Firestore.instance.collection(path);
      reference.document('1').updateData({'description': 'Head First Flutter'});
    } catch (e) {
      print(e.toString());
    }
  }

  void deleteData(path, code) {
    try {
      final reference = Firestore.instance.collection(path);
      reference.document('1').delete();
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> createData({
    String path,
    Map<String, dynamic> data,
  }) async =>
      await _setData(
        path: path,
        data: data,
      );
  Future<void> createPushData({
    String path,
    Map<String, dynamic> data,
  }) async =>
      await _pushData(
        collectionPath: path,
        data: data,
      );

  Future<QuerySnapshot> readData({
    String path,
  }) async {
    final reference = Firestore.instance.collection(path);
    final snapshot = await reference.getDocuments();

    return snapshot;
  }
}
