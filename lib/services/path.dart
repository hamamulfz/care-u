class PathFirestore {
  static String riwayatSakit(uid, id) => "users/$uid/riwayatsakit/$id";
  static String riwayatFaskes(uid, id) => "users/$uid/riwayatfaskes/$id";
  static String riwayatObat(uid, id) => "users/$uid/riwayatobat/$id";
  static String riwayatAgenda(uid, id) => "users/$uid/agenda/$id";
  static String riwayatCekGula(uid, id) => "users/$uid/guladarah/$id";
  static String riwayatCekTensi(uid, id) => "users/$uid/tensidarah/$id";

  static String riwayatSakitCollection(uid) => "users/$uid/riwayatsakit";
  static String riwayatFaskesCollection(uid) => "users/$uid/riwayatfaskes";
  static String riwayatObatCollection(uid) => "users/$uid/riwayatobat";
  static String riwayatAgendaCollection(uid) => "users/$uid/agenda";
  static String riwayatCekGulaCollection(uid) => "users/$uid/guladarah";
  static String riwayatCekTensiCollection(uid) => "users/$uid/tensidarah";
}
