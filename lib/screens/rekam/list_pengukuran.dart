import 'dart:math';

import 'package:careu/models/cpy.dart';
import 'package:careu/models/tempres.dart';
import 'package:careu/screens/rekam/isian_pengukuran.dart';
import 'package:careu/screens/rekam/isian_riwayat.dart';
import 'package:careu/services/firestore.dart';
import 'package:careu/services/path.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';

class ListPengukuran extends StatefulWidget {
  final bool isGulaDarah;
  final bool isTensiDarah;
  ListPengukuran({
    this.isGulaDarah = false,
    this.isTensiDarah = false,
  });
  @override
  _ListPengukuranState createState() => _ListPengukuranState();
}

/// Sample linear data type.

class _ListPengukuranState extends State<ListPengukuran> {
  String title = "";

  String path = "";
  String uid = "";
  _setTitle() {
    if (widget.isGulaDarah) {
      title = 'Gula Darah';
      path = PathFirestore.riwayatCekGulaCollection(uid);
    } else if (widget.isTensiDarah) {
      title = 'Tensi Darah';
      path = PathFirestore.riwayatCekTensiCollection(uid);
    }
    setState(() {});
  }

  _init() async {
    final noew = DateTime.now();
    _auth = FirebaseAuth.instance;
    user = await _auth.currentUser();
    uid = user.uid;
    print(uid);
    // _firestore = Firestore.instance;
    _db = FirestoreDb(uid: uid);
    _setTitle();
    getResult();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _init();
  }

  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  FirebaseUser user;
  FirebaseAuth _auth;
  FirestoreDb _db;

  bool _isLoading = false;
  List<TempRes> filteredProductList = [];
  ScrollController _scrollController;

  QuerySnapshot result;
  List<TempRes> _prodList = [];

  FirebaseUser currentUser;
  Firestore _firestore;

  TextEditingController _searchProductController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _showInformationSnackbar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () {
          _scaffoldKey.currentState.hideCurrentSnackBar();
          // Some code to undo the change.
        },
      ),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  getResult() async {
    // bool isHaveConnection = await CheckConnectivity.checkConnectivity();
    // if (!isHaveConnection) {
    //   _showInformationSnackbar('No Internet');
    //   return;
    // }
    filteredProductList.clear();
    _prodList.clear();
    _isLoading = true;
    setState(() {});
    result = await _db.readData(path: path);
    result.documents.forEach((f) {
      TempRes newProd = new TempRes.fromJson(f.data);
      _prodList.add(newProd);
    });
    print(_prodList.length);

    filteredProductList.addAll(_prodList);
    _isLoading = false;
    setState(() {});
  }

  filterProduct(String query) {
    _isLoading = true;
    setState(() {});
    if (query.isEmpty) {
      print('query is empty');
      filteredProductList.clear();
      filteredProductList.addAll(_prodList);
    } else {
      print('query is not empty');
      filteredProductList.clear();
      _prodList.forEach((data) {
        if (data.name.contains(query) ||
            data.name.toLowerCase().contains(query.toLowerCase())) {
          print('data found : ${data.name}');
          filteredProductList.add(data);
        }
      });
    }
    _isLoading = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var data = [
      ClicksPerYear('2016', 12, Colors.red),
      ClicksPerYear('2017', 42, Colors.yellow),
      ClicksPerYear('2018', _counter, Colors.green),
    ];

    var series = [
      new charts.Series(
        id: 'Clicks',
        domainFn: (ClicksPerYear clickData, _) => clickData.year,
        measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
        colorFn: (ClicksPerYear clickData, _) => clickData.color,
        data: data,
      ),
    ];

    /// Create random data.
    List<charts.Series<LinearSales, num>> _createRandomData() {
      final random = new Random();

      final myFakeDesktopData = [
        new LinearSales(0, random.nextInt(100)),
        new LinearSales(1, random.nextInt(100)),
        new LinearSales(2, random.nextInt(100)),
        new LinearSales(3, random.nextInt(100)),
      ];
      var myFakeMobileData = [
        new LinearSales(0, random.nextInt(100)),
        new LinearSales(1, random.nextInt(100)),
        new LinearSales(2, random.nextInt(100)),
        new LinearSales(3, random.nextInt(100)),
      ];

      return [
        new charts.Series<LinearSales, int>(
          id: 'a',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (LinearSales sales, _) => sales.year,
          measureFn: (LinearSales sales, _) => sales.sales,
          data: myFakeDesktopData,
        ),
        new charts.Series<LinearSales, int>(
          id: 'b',
          colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
          dashPatternFn: (_, __) => [8, 3, 2, 3],
          domainFn: (LinearSales sales, _) => sales.year,
          measureFn: (LinearSales sales, _) => sales.sales,
          data: myFakeMobileData,
        )
      ];
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(title ?? ""),
      ),
      body: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 50,
                  width: 50,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(width: 1, color: Colors.grey),
                  ),
                  child: TextField(
                    controller: _searchProductController,
                    onChanged: (va) => filterProduct(va),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        tooltip: 'Cari barang dari daftar yang ada',
                        icon: Icon(
                          Icons.search,
                        ),
                        onPressed: () {},
                      ),
                      hintText: 'Search',
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.all(10),
                    ),
                  ),
                ),
              ),
              // IconButton(
              //   tooltip: 'Search by Barcode',
              //   icon: Icon(Icons.scanner),
              //   onPressed: () {
              //     // _scan();
              //   },
              // )
            ],
          ),
          Expanded(
            child: new SizedBox(
              height: 200.0,
              child: charts.LineChart(
                _createRandomData(),
                animate: true,
                behaviors: [new charts.SeriesLegend()],
              ),
            ),
          ),
          if (!_isLoading)
            Center(
              child: Text(
                'Showing ${filteredProductList.length} Products',
                style: TextStyle(color: Colors.white),
              ),
            ),
          _isLoading
              ? Expanded(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              // : filteredProductList.length == 0
              //     ? Expanded(
              //         flex: 3,
              //         child: Center(
              //           child: Text('No Data'),
              //         ),
              //       )
              : Expanded(
                  flex: 2,
                  child: LiquidPullToRefresh(
                    onRefresh: () {
                      return getResult();
                    },
                    child: ListView.builder(
                      controller: _scrollController,
                      itemCount: filteredProductList.length + 1,
                      physics: BouncingScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        if (filteredProductList.length == 0) {
                          return Center(child: Text('No Data'));
                        } else if (index == filteredProductList.length) {
                          return Center(child: Text('End of list'));
                        }
                        return ExpansionTile(
                          title: Text(filteredProductList[index].date),
                          children: <Widget>[
                            Card(
                              elevation: 8,
                              margin: new EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 6.0),
                              child: Container(
                                // margin: EdgeInsets.symmetric(horizontal: 10),

                                decoration: BoxDecoration(
                                    // color: Color.fromRGBO(64, 75, 96, .9),
                                    // borderRadius: BorderRadius.circular(15),
                                    // border:
                                    //     Border.all(width: 2, color: Colors.grey)
                                    ),
                                child: ListTile(
                                  // contentPadding: EdgeInsets.all(0),
                                  title: Text(
                                    filteredProductList[index].name,
                                    style: TextStyle(
                                      // color: Colors.white,
                                      // fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  subtitle: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        filteredProductList[index].detail,
                                        style: TextStyle(
                                            // color: Colors.white,
                                            // fontSize: 15,
                                            ),
                                      ),
                                      Text(
                                        filteredProductList[index].note,
                                        style: TextStyle(
                                            // color: Colors.white,
                                            // fontSize: 15,
                                            ),
                                      ),
                                    ],
                                  ),
                                  // trailing: Container(
                                  //   decoration: BoxDecoration(
                                  //       border: new Border(
                                  //           left: new BorderSide(
                                  //               width: 1.0,
                                  //               color: Colors.white24))),
                                  //   child: Builder(
                                  //     builder: (BuildContext context) => IconButton(
                                  //       tooltip: 'Tambahkan ke Produk Saya',
                                  //       icon: Icon(
                                  //         Icons.add_circle,
                                  //         color: Colors.white,
                                  //       ),
                                  //       onPressed: () async {
                                  //         Product product =
                                  //             filteredProductList[index];

                                  //         product.price = 0;
                                  //         product.stock = 0;

                                  //         await ProductProvider.insert(product);
                                  //         Scaffold.of(context)
                                  //             .showSnackBar(SnackBar(
                                  //           duration: Duration(milliseconds: 600),
                                  //           content: Text(
                                  //               'added to your products :\n${filteredProductList[index].name}'),
                                  //         ));
                                  //       },
                                  //     ),
                                  //   ),
                                  // ),
                                ),
                              ),
                            )
                          ],
                        );
                      },
                    ),
                  ),
                ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => IsianPengkuranScreen(
              isGulaDarah: widget.isGulaDarah,
              isTensiDarah: widget.isTensiDarah,
            ),
          ));
        },
      ),
    );
  }
}
