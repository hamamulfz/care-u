import 'package:careu/screens/rekam/isian_riwayat.dart';
import 'package:careu/screens/rekam/list_pengukuran.dart';
import 'package:careu/screens/rekam/list_riwayat.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RekamMedisHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Rekam Medisku'),
        ),
        body: GridView.count(
          childAspectRatio: 1.1,
          crossAxisCount: 2,
          children: <Widget>[
            RekamHomeGrid(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      ListRiwayat(isRiwayatSakit: true),
                ));
              },
              icon: FontAwesomeIcons.lungs,
              title: 'Riwayat Sakit',
            ),
            RekamHomeGrid(
              icon: FontAwesomeIcons.hospital,
              title: 'Riwayat Faskes',
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      ListRiwayat(isRiwayatFaskes: true),
                ));
              },
            ),
            RekamHomeGrid(
              icon: FontAwesomeIcons.capsules,
              title: 'Riwayat Obat',
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      ListRiwayat(isRiwayatObat: true),
                ));
              },
            ),
            RekamHomeGrid(
              icon: FontAwesomeIcons.calendar,
              title: 'Agenda',
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      ListRiwayat(isAgenda: true),
                ));
              },
            ),
            RekamHomeGrid(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => ListPengukuran(
                    isGulaDarah: true,
                  ),
                ));
              },
              icon: FontAwesomeIcons.weight,
              title: 'Gula darah',
            ),
            RekamHomeGrid(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => ListPengukuran(
                    isTensiDarah: true,
                  ),
                ));
              },
              icon: FontAwesomeIcons.weight,
              title: 'Tensi darah',
            ),
          ],
        ));
  }
}

class RekamHomeGrid extends StatelessWidget {
  final String title;
  final IconData icon;
  final Function onTap;

  const RekamHomeGrid({
    this.icon,
    this.onTap,
    this.title,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          elevation: 10,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  // padding: EdgeInsets.only(right: 15),
                  child: Icon(
                    icon,
                    size: 80,
                    color: Colors.green,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    )),
                width: double.infinity,
                padding: EdgeInsets.all(10),
                child: Center(
                  child: Text(
                    title,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 13,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
