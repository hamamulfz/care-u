import 'package:careu/services/firestore.dart';
import 'package:careu/services/path.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class IsianPengkuranScreen extends StatefulWidget {
  final bool isGulaDarah;
  final bool isTensiDarah;
  IsianPengkuranScreen({
    this.isGulaDarah = false,
    this.isTensiDarah = false,
  });
  @override
  _IsianPengkuranScreenState createState() => _IsianPengkuranScreenState();
}

class _IsianPengkuranScreenState extends State<IsianPengkuranScreen> {
  String title = "";
  String isianSatu = "";
  String isianDua = "";
  String isianTiga = "";
  _setTitle() {
    if (widget.isGulaDarah) {
      title = 'Input Gula Darah';
      isiantanggal = 'Tanggal Pemeriksaan';
      isianDua = ' Kadar Gula Darah';
      isianTiga = 'Pengukuran Lain';
      path = PathFirestore.riwayatCekGulaCollection(uid);
    } else if (widget.isTensiDarah) {
      title = 'Input Tensi Darah';

      isiantanggal = 'Tanggal Pemeriksaan';
      isianDua = ' Nilai Sistolik';
      isianTiga = 'Nilai Diastolik';
      path = PathFirestore.riwayatCekTensiCollection(uid);
    }
    setState(() {});
  }

  String isiantanggal = "";
  String _dateController = "";
  String _timeController = "";
  String path = '';
  String uid = '';
  DateTime date;
  TimeOfDay time;
  int sistol = 110;
  int distol = 80;
  TextEditingController _nameController;
  TextEditingController _detailController;
  TextEditingController _noteController = TextEditingController();
  String get _name => _nameController.text;
  String get _detail => _detailController.text;
  String get _note => _noteController.text;
  FirebaseUser user;
  FirebaseAuth _auth;
  FirestoreDb _db;

  _init() async {
    final noew = DateTime.now();
    time = TimeOfDay(hour: noew.hour, minute: noew.minute);
    date = DateTime.now();
    _auth = FirebaseAuth.instance;
    user = await _auth.currentUser();
    uid = user.uid;
    // print(uid);
    _db = FirestoreDb(uid: uid);

    _nameController = TextEditingController(text: '$sistol');
    _detailController = TextEditingController(text: '$distol');
    _setTitle();
    setState(() {});
  }

  _formatDate(DateTime date) {
    var format = DateFormat('yyyy-MM-dd');
    var dateString = format.format(date);
    return dateString;
  }

  _formatTime(TimeOfDay time) {
    return '${time.hour}:${time.minute}';
  }

  Map<String, dynamic> _createBody() {
    return {
      "date": _formatDate(date),
      "time": _formatTime(time),
      "name": _name,
      "detail": _detail,
      "note": _note
    };
  }

  _increment(type) {
    // num += 1;
    if (type) {
      sistol += 1;
      _nameController.text = '$sistol';
    } else {
      distol += 1;
      _detailController.text = '$distol';
    }

    setState(() {});
  }

  _decrement(type) {
    // num -= 1;
    if (type) {
      sistol -= 1;
      _nameController.text = '$sistol';
    } else {
      distol -= 1;
      _detailController.text = '$distol';
    }

    setState(() {});
  }

  onChange(type, String text) {
    // num -= 1;
    int number = int.parse(text);
    if (type) {
      sistol = int.parse(_nameController.text);
    } else {
      distol = int.parse(_detailController.text);
    }

    setState(() {});
  }

  _submit() {
    // print(uid);
    // print(path);
    // print(_createBody());
    _db.createPushData(
      path: path,
      data: _createBody(),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _init();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _nameController.dispose();
    _detailController.dispose();
    _noteController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
          style: TextStyle(color: Colors.black),
        ),
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(isiantanggal),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Material(
                      elevation: 10,
                      child: InkWell(
                        onTap: () async {
                          final a = await showDatePicker(
                              context: context,
                              firstDate: DateTime(2018),
                              initialDate: DateTime.now(),
                              lastDate: DateTime(2030));
                          if (a != null) date = a;
                        },
                        child: Container(
                          padding: EdgeInsets.all(10),
                          decoration:
                              BoxDecoration(border: Border.all(width: 1)),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: Text(
                                _formatDate(date),
                                textAlign: TextAlign.center,
                              )),
                              Icon(Icons.calendar_today)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: Material(
                      elevation: 10,
                      child: InkWell(
                        onTap: () async {
                          final now = DateTime.now();
                          final a = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay(
                              hour: now.hour,
                              minute: now.minute,
                            ),
                          );

                          if (a != null) time = a;
                          print(a);
                        },
                        child: Container(
                          padding: EdgeInsets.all(10),
                          decoration:
                              BoxDecoration(border: Border.all(width: 1)),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: Text(
                                _formatTime(time),
                                textAlign: TextAlign.center,
                              )),
                              Icon(Icons.av_timer)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 25),
              RiwayatSakitSection(
                title: isianDua,
                onTapIncrement: () => _increment(true),
                onTapDecrement: () => _decrement(true),
                controller: _nameController,
                onChange: onChange,
                isSistol: true,
              ),
              RiwayatSakitSection(
                title: isianTiga,
                controller: _detailController,
                onTapIncrement: () => _increment(false),
                onTapDecrement: () => _decrement(false),
                onChange: onChange,
                isSistol: false,
              ),
              // RiwayatSakitSection(
              //   title: 'Spesialisasi',
              // ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    maxLines: 6,
                    controller: _noteController,
                    decoration: InputDecoration(
                      hintText: 'Catatan',
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                child: RaisedButton.icon(
                  color: Colors.green,
                  icon: Icon(Icons.save, color: Colors.white),
                  label: Text(
                    'Simpan',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () async {
                    print(sistol);
                    print(distol);
                    // await _submit();
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class RiwayatSakitSection extends StatelessWidget {
  final String title;
  final bool isSistol;
  final Function onTap;
  final Function(bool, String) onChange;
  final Function onTapDecrement;
  final Function onTapIncrement;
  final TextEditingController controller;
  const RiwayatSakitSection({
    Key key,
    this.title,
    this.isSistol,
    this.onTap,
    this.onChange,
    this.onTapIncrement,
    this.onTapDecrement,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(title),
            Card(
              elevation: 5,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.minimize),
                      onPressed: onTapDecrement,
                    ),
                    Expanded(child: Container()),
                    Expanded(
                      child: TextField(
                        controller: controller,
                        onChanged: (val) => onChange(isSistol, val),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.grey),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    Expanded(child: Container()),
                    IconButton(
                      icon: Icon(Icons.add),
                      onPressed: onTapIncrement,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
