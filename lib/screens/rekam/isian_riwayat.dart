import 'dart:convert';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:careu/models/icd10.dart';
import 'package:careu/services/firestore.dart';
import 'package:careu/services/path.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class IsianRiwayatScreen extends StatefulWidget {
  final bool isRiwayatSakit;
  final bool isRiwayatFaskes;
  final bool isRiwayatObat;
  final bool isAgenda;
  IsianRiwayatScreen({
    this.isRiwayatFaskes = false,
    this.isRiwayatObat = false,
    this.isRiwayatSakit = false,
    this.isAgenda = false,
  });
  @override
  _IsianRiwayatScreenState createState() => _IsianRiwayatScreenState();
}

class _IsianRiwayatScreenState extends State<IsianRiwayatScreen> {
  String title = "";
  String isiantanggal = "";
  String isianDua = "";
  String isianTiga = "";
  String _dateController = "";
  String _timeController = "";
  String path = '';
  String uid = '';
  DateTime date;
  TimeOfDay time;
  TextEditingController _nameController = TextEditingController();
  TextEditingController _detailController = TextEditingController();
  TextEditingController _noteController = TextEditingController();
  String get _name => _nameController.text;
  String get _detail => _detailController.text;
  String get _note => _noteController.text;
  FirebaseUser user;
  FirebaseAuth _auth;
  FirestoreDb _db;
  String currentText = "";
  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();

  _init() async {
    final noew = DateTime.now();
    time = TimeOfDay(hour: noew.hour, minute: noew.minute);
    date = DateTime.now();
    _auth = FirebaseAuth.instance;
    user = await _auth.currentUser();
    uid = user.uid;
    // print(uid);
    _db = FirestoreDb(uid: uid);
    _setTitle();
    setState(() {});
  }

  _formatDate(DateTime date) {
    var format = DateFormat('yyyy-MM-dd');
    var dateString = format.format(date);
    return dateString;
  }

  _formatTime(TimeOfDay time) {
    return '${time.hour}:${time.minute}';
  }

  Map<String, dynamic> _createBody() {
    return {
      "date": _formatDate(date),
      "time": _formatTime(time),
      "code": _codeIcdController.text,
      "name": _name,
      "detail": _detail,
      "note": _note
    };
  }

  _submit() {
    // print(uid);
    // print(path);
    // print(_createBody());
    _db.createPushData(
      path: path,
      data: _createBody(),
    );
  }

  _setTitle() {
    if (widget.isRiwayatSakit) {
      title = 'Riwayat Sakit';

      isiantanggal = 'Tanggal Sakit';
      isianDua = ' Nama Diagnosis';
      isianTiga = 'Faskes Dikunjungi';
      path = PathFirestore.riwayatSakitCollection(uid);
    } else if (widget.isRiwayatFaskes) {
      title = 'Riwayat Faskes';

      isiantanggal = 'Tanggal Kunjungan';
      isianDua = ' Nama Faskes';
      isianTiga = 'Agenda';
      path = PathFirestore.riwayatFaskesCollection(uid);
    } else if (widget.isRiwayatObat) {
      title = 'Riwayat Obat';

      isiantanggal = 'Tanggal Pemberian Obat';
      isianDua = ' Nama Obat';
      isianTiga = 'Dosis';
      path = PathFirestore.riwayatObatCollection(uid);
    } else if (widget.isAgenda) {
      title = 'Agenda';

      isiantanggal = 'Jadwal Kontrol';
      isianDua = ' Faskes';
      isianTiga = 'Agenda';
      path = PathFirestore.riwayatAgendaCollection(uid);
    }
    setState(() {});
  }

  List<String> suggestions = [];
  List<String> added = [];
  TextEditingController _codeIcdController = TextEditingController();
  _getList() async {
    final a = await loadContainerListBuilder();
    suggestions.addAll(a);
  }

  Future<List<String>> loadContainerListBuilder() async {
    return await rootBundle
        .loadString('assets/kode.json')
        .then((String news) => jsonDecode(news) as Map<String, dynamic>)
        .then(
      (value) {
        List<String> listFlags = [];

        Icd10 list = Icd10.fromJson(value);

        list.kodeIcd10
            .forEach((i) => listFlags.add("${i.kodeICD10} - ${i.diskripsi}"));

        return listFlags;
      },
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _init();
    _getList();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _nameController.dispose();
    _detailController.dispose();
    _noteController.dispose();
    _codeIcdController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final textField = SimpleAutoCompleteTextField(
        key: key,
        // decoration: new InputDecoration(errorText: "Beans"),
        controller: _codeIcdController,
        suggestions: suggestions,
        textChanged: (text) => currentText = text,
        decoration: InputDecoration(
          hintText: "Kode ICD",
          border: InputBorder.none,
        ),
        clearOnSubmit: true,
        textSubmitted: (text) => setState(() {
              if (text != "") {
                added.add(text);
              }
            }));

    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
          style: TextStyle(color: Colors.black),
        ),
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(isiantanggal),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Material(
                      borderRadius: BorderRadius.circular(10),
                      elevation: 10,
                      child: InkWell(
                        borderRadius: BorderRadius.circular(10),
                        onTap: () async {
                          final a = await showDatePicker(
                              context: context,
                              firstDate: DateTime(2018),
                              initialDate: DateTime.now(),
                              lastDate: DateTime(2030));
                          if (a != null) date = a;
                        },
                        child: Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            // border: Border.all(width: 1),
                            borderRadius: BorderRadius.circular(10),
                            color: Color(0xffebebeb),
                          ),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: Text(
                                _formatDate(date),
                                textAlign: TextAlign.center,
                              )),
                              Icon(Icons.calendar_today)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 15),
                  if (widget.isAgenda)
                    Expanded(
                      child: Material(
                        elevation: 10,
                        borderRadius: BorderRadius.circular(10),
                        child: InkWell(
                          borderRadius: BorderRadius.circular(10),
                          onTap: () async {
                            final now = DateTime.now();
                            final a = await showTimePicker(
                              context: context,
                              initialTime: TimeOfDay(
                                hour: now.hour,
                                minute: now.minute,
                              ),
                            );

                            if (a != null) time = a;
                            print(a);
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              // border: Border.all(width: 1),
                              borderRadius: BorderRadius.circular(10),
                              color: Color(0xffebebeb),
                            ),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                    child: Text(
                                  _formatTime(time),
                                  textAlign: TextAlign.center,
                                )),
                                Icon(Icons.av_timer)
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                ],
              ),

              SizedBox(height: 25),
              Container(
                width: double.infinity,
                child: Card(
                  
                  elevation: 10,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(child: textField),
                        Icon(Icons.edit),
                      ],
                    ),
                  ),
                ),
              ),
              RiwayatSakitSection(
                title: isianDua,
                controller: _nameController,
              ),
              RiwayatSakitSection(
                title: isianTiga,
                controller: _detailController,
              ),
              // RiwayatSakitSection(
              //   title: 'Spesialisasi',
              // ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    maxLines: 6,
                    controller: _noteController,
                    decoration: InputDecoration(
                      hintText: 'Catatan',
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                height: 40,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  color: Colors.green,
                  // icon: Icon(Icons.save, color: Colors.white),
                  child: Text(
                    'Simpan',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () async {
                    await _submit();
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class RiwayatSakitSection extends StatelessWidget {
  final String title;
  final Function onTap;
  final TextEditingController controller;
  const RiwayatSakitSection({
    Key key,
    this.title,
    this.onTap,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        child: Card(
          elevation: 10,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: controller,
                    style: TextStyle(color: Colors.grey),
                    decoration: InputDecoration(
                      hintText: title,
                      border: InputBorder.none,
                    ),
                  ),
                ),
                Icon(Icons.edit),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
