import 'package:careu/screens/auth/login_screen.dart';
import 'package:flutter/services.dart';
import 'package:careu/screens/main_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passworController = TextEditingController();
  String get _email => _emailController.text;
  String get _password => _passworController.text;
  FirebaseAuth _firebaseAuth;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  _login() {
    try {
      return _firebaseAuth.signInWithEmailAndPassword(
          email: _email, password: _password);
    } on PlatformException catch (e) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(e.message), duration: Duration(milliseconds: 600)));
    }
  }

  _showError(message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(message), duration: Duration(milliseconds: 600)));
  }

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _firebaseAuth = FirebaseAuth.instance;
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      progressIndicator: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation(Colors.white),
      ),
      inAsyncCall: _isLoading,
      child: Scaffold(
        // backgroundColor: Colors.green,
        key: _scaffoldKey,
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.white,
                Color(0xffF3F4F6),
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: SafeArea(
              child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios),
                        Text(
                          'Back',
                          style: TextStyle(
                              color: Color(0xff4f4f4f),
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                  Column(
                    children: [
                      Text(
                        'DAFTAR',
                        style: TextStyle(
                            color: Color(0xff4f4f4f),
                            fontWeight: FontWeight.bold,
                            fontSize: 26),
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                  Text(
                    'Email anda',
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xff4f4f4f),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    // padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadiusDirectional.circular(5),
                      boxShadow: [
                        BoxShadow(
                            offset: Offset(4, 10),
                            blurRadius: 40,
                            color: Colors.black.withOpacity(0.25))
                      ],
                    ),
                    child: TextField(
                      controller: _emailController,
                      decoration: InputDecoration(
                        hintText: "email@email.com",
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 4,
                              color: Colors.white,
                              style: BorderStyle.none),
                        ),
                        suffixIcon: Icon(Icons.person),
                      ),
                    ),
                  ),
                  SizedBox(height: 30),
                  Text(
                    'Kata sandi',
                    style: TextStyle(
                      color: Color(0xff4f4f4f),
                      fontSize: 14,
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadiusDirectional.circular(5),
                      boxShadow: [
                        BoxShadow(
                            offset: Offset(4, 10),
                            blurRadius: 40,
                            color: Colors.black.withOpacity(0.25))
                      ],
                    ),
                    child: TextField(
                      obscureText: true,
                      controller: _passworController,
                      decoration: InputDecoration(
                        hintText: "****",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(width: 0)),
                        suffixIcon: Icon(Icons.vpn_key),
                      ),
                    ),
                  ),
                  SizedBox(height: 30),
                  Container(
                    width: double.infinity,
                    height: 40,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        'DAFTAR',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      color: Color(0xff27AE60),
                      onPressed: () async {
                        if (_email.isEmpty || _password.isEmpty) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content:
                                  Text('Email dan password tidak boleh kosong'),
                              duration: Duration(milliseconds: 600)));
                          // _showError('Email dan password tidak boleh kosong');
                          return;
                        }
                        _isLoading = true;
                        setState(() {});
                        AuthResult auth;
                        print(_email);
                        try {
                          auth = await _firebaseAuth
                              .createUserWithEmailAndPassword(
                                  email: _email.trim(), password: _password.trim());
                        } on PlatformException catch (e) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text(e.message),
                          ));
                        }
                        if (auth?.user != null) {
                          auth.user.sendEmailVerification();
                          print(auth.user.uid);
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text(
                                'Berhasil membuat akun',
                              ),
                              action: SnackBarAction(
                                label: 'Masuk',
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              )));
                          // Navigator.of(context).push(MaterialPageRoute(
                          //     builder: (context) => MainScreen()));
                        }

                        _isLoading = false;
                        setState(() {});
                      },
                    ),
                  ),

                  SizedBox(height: 10),
                  Center(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.6,
                      child: Text(
                        'Dengan klik daftar, Anda akan menyetujui Syarat dan ketentuan serta kebijakan privacy kami',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 10,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ),
                  // SizedBox(height: 10),
                  // Center(
                  //   child: FlatButton(
                  //     child: Text(
                  //       'Sudah memiliki akun? Masuk',
                  //       textAlign: TextAlign.center,
                  //       style: TextStyle(color: Colors.white),
                  //     ),
                  //     onPressed: () {
                  //       Navigator.pop(context);
                  //     },
                  //   ),
                  // ),
                ],
              ),
            ),
          )),
        ),
      ),
    );
  }
}
