// import 'package:careu/screens/auth/login_screen.dart';
// import 'package:careu/screens/auth/register_screen.dart';
// import 'package:flutter/material.dart';

// class WelcomeScreen extends StatefulWidget {
//   WelcomeScreen({Key key, this.title}) : super(key: key);

//   final String title;

//   @override
//   _WelcomeScreenState createState() => _WelcomeScreenState();
// }

// class _WelcomeScreenState extends State<WelcomeScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Theme.of(context).primaryColor,
//       // appBar: AppBar(
//       //   title: Text(widget.title),
//       // ),
//       body: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: <Widget>[
//           Expanded(
//             flex: 6,
//             child: Center(
//               child: CircleAvatar(
//                 backgroundColor: Colors.white10,
//                 radius: MediaQuery.of(context).size.width * 0.4,
//                 child: CircleAvatar(
//                   backgroundColor: Colors.white10,
//                   radius: MediaQuery.of(context).size.width * 0.35,
//                   child: CircleAvatar(
//                     backgroundColor: Colors.white10,
//                     radius: MediaQuery.of(context).size.width * 0.3,
//                     child: CircleAvatar(
//                       backgroundColor: Colors.white54,
//                       radius: MediaQuery.of(context).size.width * 0.25,
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ),
//           Expanded(
//             flex: 2,
//             child: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 20),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.stretch,
//                 children: <Widget>[
//                   Material(
//                     child: InkWell(
//                       onTap: () {
//                         Navigator.of(context).push(MaterialPageRoute(
//                             builder: (BuildContext context) => LoginScreen()));
//                       },
//                       child: Card(
//                         elevation: 10,
//                         child: Center(
//                           child: Padding(
//                             padding: const EdgeInsets.all(10.0),
//                             child: Text(
//                               'LOGIN',
//                               style: TextStyle(
//                                   color: Theme.of(context).primaryColor,
//                                   fontWeight: FontWeight.bold,
//                                   letterSpacing: 2,
//                                   fontSize: 20),
//                             ),
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 10),
//                   Material(
//                     child: InkWell(
//                       onTap: () {
//                         Navigator.of(context).push(MaterialPageRoute(
//                             builder: (BuildContext context) =>
//                                 RegisterScreen()));
//                       },
//                       child: Card(
//                         elevation: 10,
//                         child: Center(
//                             child: Padding(
//                           padding: const EdgeInsets.all(10.0),
//                           child: Text(
//                             'REGISTER',
//                             style: TextStyle(
//                                 color: Theme.of(context).primaryColor,
//                                 fontWeight: FontWeight.bold,
//                                 letterSpacing: 2,
//                                 fontSize: 20),
//                           ),
//                         )),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
