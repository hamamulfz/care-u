import 'package:careu/screens/auth/register_screen.dart';
import 'package:flutter/services.dart';
import 'package:careu/screens/main_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passworController = TextEditingController();
  String get _email => _emailController.text;
  String get _password => _passworController.text;
  FirebaseAuth _firebaseAuth;
  bool _isLoading = false;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  _login() {
    try {
      return _firebaseAuth.signInWithEmailAndPassword(
          email: _email, password: _password);
    } on PlatformException catch (e) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(e.message), duration: Duration(milliseconds: 600)));
    }
  }

  _showError(message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(message), duration: Duration(milliseconds: 600)));
  }

  @override
  void initState() {
    super.initState();
    _firebaseAuth = FirebaseAuth.instance;
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: _isLoading,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.5,
              width: double.infinity,
              child: Image.asset(
                'assets/header.png',
                fit: BoxFit.cover,
              ),
            ),
            Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.35,
                ),
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.white,
                        Color(0xffF3F4F6),
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40)),
                  ),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.65,
                  child: Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Column(
                      children: [
                        Center(
                          child: Text(
                            'LOGIN',
                            style: TextStyle(
                                color: Colors.black.withOpacity(0.5),
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ),
                        SizedBox(height: 20),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadiusDirectional.circular(5),
                            boxShadow: [
                              BoxShadow(
                                  offset: Offset(4, 10),
                                  blurRadius: 40,
                                  color: Colors.black.withOpacity(0.25))
                            ],
                          ),
                          child: TextField(
                            controller: _emailController,
                            decoration: InputDecoration(
                              hintText: "Email",
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(width: 1)),
                              suffixIcon: Icon(Icons.person),
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadiusDirectional.circular(5),
                            boxShadow: [
                              BoxShadow(
                                  offset: Offset(4, 10),
                                  blurRadius: 40,
                                  color: Colors.black.withOpacity(0.25))
                            ],
                          ),
                          child: TextField(
                            obscureText: true,
                            controller: _passworController,
                            decoration: InputDecoration(
                              hintText: "Password",
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(width: 1)),
                              suffixIcon: Icon(Icons.vpn_key),
                            ),
                          ),
                        ),
                        // SizedBox(height: 10),
                        // Center(
                        //   child: SizedBox(
                        //     width: MediaQuery.of(context).size.width * 0.6,
                        //     child: Text(
                        //       'Dengan klik masuk, Anda akan menyetujui Syarat dan ketentuan serta kebijakan privacy kami',
                        //       textAlign: TextAlign.center,
                        //       style: TextStyle(fontSize: 10),
                        //     ),
                        //   ),
                        // ),
                        SizedBox(height: 30),
                        Container(
                          width: double.infinity,
                          height: 50,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5)),
                            child: Text(
                              'MASUK',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            color: Theme.of(context).primaryColor,
                            onPressed: () async {
                              if (_email.isEmpty || _password.isEmpty) {
                                _showError(
                                    'Email dan password tidak boleh kosong');
                                return;
                              }

                              _isLoading = true;
                              setState(() {});
                              AuthResult auth;
                              try {
                                auth = await _firebaseAuth
                                    .signInWithEmailAndPassword(
                                        email: _email, password: _password);
                              } on PlatformException catch (e) {
                                _scaffoldKey.currentState.showSnackBar(SnackBar(
                                  content: Text(e.message),
                                ));
                              }
                              if (auth?.user != null) {
                                print(auth.user.uid);
                                final _prefs =
                                    await SharedPreferences.getInstance();
                                await _prefs.setString('user', auth.user.uid);
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (context) => MainScreen()));
                              }

                              _isLoading = false;
                              setState(() {});
                            },
                          ),
                        ),
                        SizedBox(height: 10),
                        // Center(
                        //   child: Text(
                        //     'Lupa Password?',
                        //     textAlign: TextAlign.center,
                        //   ),
                        // ),
                        // SizedBox(height: 10),
                        Center(
                          child: FlatButton(
                            child: Text(
                              'Belum memiliki akun? Daftar',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.grey.withOpacity(0.8)),
                            ),
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      RegisterScreen()));
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<SafeArea> buildSafeArea(BuildContext context) async {
    return SafeArea(
        child: Stack(
      children: [
        SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * 0.1),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.2,
                      child: Image.asset('assets/logo.png'),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Care ',
                                style: TextStyle(
                                    color: Colors.lightGreen,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 60),
                              ),
                              TextSpan(
                                text: 'U',
                                style: TextStyle(
                                    color: Colors.green,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 60),
                              ),
                            ],
                          ),
                        ),
                        Text(
                          'your personal healthcare companion',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 10,
                            fontStyle: FontStyle.italic,
                          ),
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                Center(
                  child: Text(
                    'LOGIN',
                    style: TextStyle(
                        color: Colors.lightGreen,
                        fontWeight: FontWeight.bold,
                        fontSize: 30),
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                Text('Email anda'),
                TextField(
                  controller: _emailController,
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.person),
                  ),
                ),
                SizedBox(height: 30),
                Text('Kata sandi'),
                TextField(
                  obscureText: true,
                  controller: _passworController,
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.vpn_key),
                  ),
                ),
                SizedBox(height: 10),
                Center(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Text(
                      'Dengan klik masuk, Anda akan menyetujui Syarat dan ketentuan serta kebijakan privacy kami',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 10),
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Center(
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40)),
                    child: Text(
                      'MASUK',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    color: Theme.of(context).primaryColor,
                    onPressed: () async {
                      if (_email.isEmpty || _password.isEmpty) {
                        _showError('Email dan password tidak boleh kosong');
                        return;
                      }

                      _isLoading = true;
                      setState(() {});
                      AuthResult auth;
                      try {
                        auth = await _firebaseAuth.signInWithEmailAndPassword(
                            email: _email, password: _password);
                      } on PlatformException catch (e) {
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text(e.message),
                        ));
                      }
                      if (auth?.user != null) {
                        print(auth.user.uid);
                        final _prefs = await SharedPreferences.getInstance();
                        await _prefs.setString('user', auth.user.uid);
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => MainScreen()));
                      }

                      _isLoading = false;
                      setState(() {});
                    },
                  ),
                ),
                SizedBox(height: 10),
                Center(
                  child: Text(
                    'Lupa Password?',
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 10),
                Center(
                  child: FlatButton(
                    child: Text(
                      'Belum memiliki akun? Daftar',
                      textAlign: TextAlign.center,
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => RegisterScreen()));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }
}
