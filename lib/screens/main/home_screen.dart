import 'package:careu/screens/dokter/dokter_list_page.dart';
import 'package:careu/screens/faskes/faskes_list_page.dart';
import 'package:careu/screens/rekam/list_pengukuran.dart';
import 'package:careu/screens/rekam/list_riwayat.dart';
import 'package:careu/screens/rekam/rekam_medis_home.dart';
import 'package:careu/widgets/careu_box_item.dart';
import 'package:careu/widgets/careu_item.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: ListView(
        children: <Widget>[
          Center(child: Text('Hai  Andika')),
          Center(child: Text('Merasa tidak sehat?')),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: CareUItem(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => RekamMedisHome(),
                      ));
                    },
                    icon: FontAwesomeIcons.heartbeat,
                    // image: 'assets/logo.png',
                    title: 'Rekam Medisku',
                  ),
                ),
              ),
              Expanded(
                child: CareUItem(
                  icon: FontAwesomeIcons.ambulance,
                  // image: 'assets/logo.png',
                  title: 'Cari Faskes',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => FaskesListPage(),
                    ));
                  },
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: CareUItem(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => DokterListPage(
                          specs: 'umum',
                        ),
                      ));
                    },
                    icon: FontAwesomeIcons.stethoscope,
                    // image: 'assets/logo.png',
                    title: 'Cari Dokter',
                  ),
                ),
              ),
              Expanded(
                child: CareUItem(
                  icon: FontAwesomeIcons.notesMedical,
                  // image: 'assets/logo.png',
                  title: 'Pembayaran',
                ),
              ),
            ],
          ),
          Divider(
            color: Colors.green,
            thickness: 2,
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Text(
              'Rekam Medisku',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.green,
                fontSize: 20,
              ),
            ),
          ),
          Container(
            height: 110,
            margin: EdgeInsets.all(3),
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                CareUBoxItem(
                  icon: FontAwesomeIcons.lungs,

                  // image: 'assets/logo.png',
                  title: 'Riwayat Sakit',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) =>
                          ListRiwayat(isRiwayatSakit: true),
                    ));
                  },
                ),
                SizedBox(width: 10),
                CareUBoxItem(
                  icon: FontAwesomeIcons.weight,

                  // image: 'assets/logo.png',
                  title: 'Pengukuran Kesehatan',
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) =>
                            ListPengukuran(isTensiDarah: true),
                      ),
                    );
                  },
                ),
                SizedBox(width: 10),
                CareUBoxItem(
                  icon: FontAwesomeIcons.capsules,

                  // image: 'assets/logo.png',
                  title: 'Resep Obat',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) =>
                          ListRiwayat(isRiwayatObat: true),
                    ));
                  },
                ),
                SizedBox(width: 10),
                CareUBoxItem(
                  icon: FontAwesomeIcons.ellipsisH,
                  // image: 'assets/logo.png',
                  title: 'Lain-lain',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => RekamMedisHome(),
                    ));
                  },
                ),
                SizedBox(width: 10),
                CareUBoxItem(
                  image: 'assets/logo.png',
                  title: 'Lihat Semua',
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.green,
            thickness: 2,
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Text(
              'Cari Faskes',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.green,
                fontSize: 20,
              ),
            ),
          ),
          Container(
            height: 110,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                CareUBoxItem(
                  icon: FontAwesomeIcons.hospital,

                  // image: 'assets/logo.png',
                  title: 'Rumah Sakit',
                ),
                SizedBox(width: 10),
                CareUBoxItem(
                  icon: FontAwesomeIcons.plus,
                  // image: 'assets/logo.png',
                  title: 'Klinik Umum',
                ),
                SizedBox(width: 10),
                CareUBoxItem(
                  icon: FontAwesomeIcons.tooth,

                  // image: 'assets/logo.png',
                  title: 'Klinik Gigi',
                ),
                SizedBox(width: 10),
                CareUBoxItem(
                  icon: FontAwesomeIcons.ellipsisH,

                  // image: 'assets/logo.png',
                  title: 'Lihat Semua',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => FaskesListPage(),
                    ));
                  },
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.green,
            thickness: 2,
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Text(
              'Cari Dokter',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.green,
                fontSize: 20,
              ),
            ),
          ),
          Container(
            height: 110,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                CareUBoxItem(
                  icon: FontAwesomeIcons.tooth,
                  // image: 'assets/logo.png',
                  title: 'Gigi',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => DokterListPage(
                        specs: 'gigi',
                        categoryNumber: 1,
                      ),
                    ));
                  },
                ),
                SizedBox(width: 10),
                CareUBoxItem(
                  icon: FontAwesomeIcons.lungs,
                  // image: 'assets/logo.png',
                  title: 'Jantung',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => DokterListPage(
                        specs: 'jantung',
                        categoryNumber: 2,
                      ),
                    ));
                  },
                ),
                SizedBox(width: 10),
                CareUBoxItem(
                  icon: FontAwesomeIcons.female,
                  // image: 'assets/logo.png',
                  title: 'Kebidanan',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => DokterListPage(
                        specs: 'umum',
                        categoryNumber: 3,
                      ),
                    ));
                  },
                ),
                SizedBox(width: 10),
                CareUBoxItem(
                  icon: FontAwesomeIcons.ellipsisH,

                  // image: 'assets/logo.png',
                  title: 'Lihat Semua',
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => DokterListPage(
                        specs: 'umum',
                      ),
                    ));
                  },
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.green,
            thickness: 2,
          ),
        ],
      ),
    );
  }
}
