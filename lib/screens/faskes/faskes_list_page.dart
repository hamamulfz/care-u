import 'package:careu/screens/faskes/faskes_detail.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class FaskesListPage extends StatefulWidget {
  @override
  _FaskesListPageState createState() => _FaskesListPageState();
}

class _FaskesListPageState extends State<FaskesListPage> {
  List<DocumentSnapshot> listDokter = [];
  List<DocumentSnapshot> listFaskes = [];
  bool isLoading = false;

  _getListFaskes() async {
    isLoading = true;
    setState(() {});
    final data = await Firestore.instance.collection('faskes').getDocuments();
    listFaskes = data.documents;
    print(listFaskes[0].data);
    isLoading = false;
    setState(() {});
  }

  Future<DocumentSnapshot> _getListDokter(id) async {
    final data =
        await Firestore.instance.document('faskes/$id/dokter-1/dokter-1').get();
    print(data.data);
    return data;
  }

  @override
  void initState() {
    super.initState();
    _getListFaskes();
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: isLoading,
      child: Scaffold(
          appBar: AppBar(
            title: Text('Faskes List'),
          ),
          body: ListView.builder(
            itemCount: listFaskes.length,
            itemBuilder: (BuildContext context, int index) {
              final data = listFaskes[index].data;
              final id = listFaskes[index].documentID;
              return Card(
                elevation: 10,
                child: ListTile(
                  leading: Icon(FontAwesomeIcons.hospital),
                  title: Text(
                    data['nama'],
                    overflow: TextOverflow.ellipsis,
                  ),
                  subtitle: Text(data['buka']),
                  onTap: () async {
                    isLoading = true;
                    setState(() {});
                    final dokter = await _getListDokter(id);
                    print(dokter.data);

                    isLoading = false;
                    setState(() {});
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => FaskesDetail(
                        faskes: listFaskes[index],
                        dokter: dokter,
                      ),
                    ));
                  },
                ),
              );
            },
          )),
    );
  }
}
