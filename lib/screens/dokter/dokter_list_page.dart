import 'package:careu/screens/faskes/faskes_detail.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class DokterListPage extends StatefulWidget {
  final String specs;
  final int categoryNumber;
  DokterListPage({
    this.specs,
    this.categoryNumber = 0,
  });
  @override
  DokterListPageState createState() => DokterListPageState();
}

const List<Map<String, dynamic>> categoryList = [
  {
    "name": "umum",
    "icons": FontAwesomeIcons.userMd,
  },
  {
    "name": "gigi",
    "icons": FontAwesomeIcons.tooth,
  },
  {
    "name": "jantung",
    "icons": FontAwesomeIcons.lungs,
  },
  {
    "name": "kebidanan",
    "icons": FontAwesomeIcons.female,
  },
  {
    "name": "anak",
    "icons": FontAwesomeIcons.child,
  },
  {
    "name": "anestesi",
    "icons": FontAwesomeIcons.syringe,
  },
  {
    "name": "bedah",
    "icons": FontAwesomeIcons.handHoldingMedical,
  },
  {
    "name": "mata",
    "icons": FontAwesomeIcons.eye,
  },
  {
    "name": "neurologi",
    "icons": FontAwesomeIcons.diagnoses,
  },
  {
    "name": "psikiater",
    "icons": FontAwesomeIcons.handHoldingHeart,
  },
  {
    "name": "psikolog",
    "icons": FontAwesomeIcons.headSideVirus,
  },
  {
    "name": "radiologi",
    "icons": FontAwesomeIcons.dna,
  },
];

class DokterListPageState extends State<DokterListPage> {
  List<DocumentSnapshot> listDokter = [];
  List<DocumentSnapshot> listFaskes = [];
  bool isLoading = false;
  int _selectedCategory = 0;

  _getListFaskes(category) async {
    isLoading = true;
    setState(() {});
    final data =
        await Firestore.instance.collection('$category').getDocuments();
    listFaskes = data.documents;
    print(listFaskes[0].data);
    isLoading = false;
    setState(() {});
  }

  Future<DocumentSnapshot> _getListDokter(id) async {
    final data =
        await Firestore.instance.document('faskes/$id/dokter-1/dokter-1').get();
    print(data.data);
    return data;
  }

  @override
  void initState() {
    super.initState();
    _getListFaskes(widget.specs);
    _selectedCategory = widget.categoryNumber ?? 0;
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: isLoading,
      child: Scaffold(
          appBar: AppBar(
            title: Text('Dokter List'),
          ),
          body: Column(
            children: <Widget>[
              Container(
                height: 100,
                child: ListView.builder(
                  itemCount: categoryList.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        _selectedCategory = index;
                        _getListFaskes(categoryList[index]['name']);
                        setState(() {});
                      },
                      child: Container(
                        width: 100,
                        height: 80,
                        child: Card(
                          color: _selectedCategory == index
                              ? Colors.green
                              : Colors.white,
                          elevation: 5,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                categoryList[index]['icons'],
                                color: _selectedCategory == index
                                    ? Colors.white
                                    : Colors.black,
                              ),
                              SizedBox(height: 10),
                              Text(
                                '${categoryList[index]['name'].toString().toUpperCase()}',
                                style: TextStyle(
                                    color: _selectedCategory == index
                                        ? Colors.white
                                        : Colors.black),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: listFaskes.length,
                  itemBuilder: (BuildContext context, int index) {
                    final data = listFaskes[index].data;
                    final id = listFaskes[index].documentID;
                    return Card(
                      elevation: 10,
                      child: ListTile(
                        leading: Icon(FontAwesomeIcons.userMd),
                        title: Text(
                          data['Nama'],
                          overflow: TextOverflow.ellipsis,
                        ),
                        subtitle: Text(data['Faskes']),
                        onTap: () async {
                          // isLoading = true;
                          // setState(() {});
                          // final dokter = await _getListDokter(id);
                          // print(dokter.data);

                          // isLoading = false;
                          // setState(() {});
                          // Navigator.of(context).push(MaterialPageRoute(
                          //   builder: (BuildContext context) => FaskesDetail(
                          //     faskes: listFaskes[index],
                          //     dokter: dokter,
                          //   ),
                          // ));
                        },
                      ),
                    );
                  },
                ),
              ),
            ],
          )),
    );
  }
}
