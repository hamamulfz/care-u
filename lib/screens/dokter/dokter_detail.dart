import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DokterDetail extends StatelessWidget {
  final DocumentSnapshot faskes, dokter;
  DokterDetail({
    this.dokter,
    this.faskes,
  });
  @override
  Widget build(BuildContext context) {
    final dokterDetail = faskes.data;
    // final dokterDetail = dokter.data;
    print((dokterDetail['map'] as GeoPoint).latitude);
    print((dokterDetail['map'] as GeoPoint).longitude);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          dokterDetail['nama'],
          overflow: TextOverflow.ellipsis,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(dokterDetail['nama']),
            SizedBox(height: 20),
            RowDataFaskes(
              keyInfo: 'Jam Buka',
              data: dokterDetail['buka'],
            ),
            RowDataFaskes(
              keyInfo: 'Kelas',
              data: dokterDetail['kelas'],
            ),
            RowDataFaskes(
              keyInfo: 'Website',
              data: dokterDetail['web'],
            ),
            RowDataFaskes(
              keyInfo: 'email',
              data: dokterDetail['email'],
            ),
            RowDataFaskes(
              keyInfo: 'alamat',
              data: dokterDetail['alamat'],
            ),
            SizedBox(height: 20),
            // Text('Daftar Dokter'),
            // Expanded(
            //   child: ListView.builder(
            //     itemCount: (dokterDetail['nama'] as List).length,
            //     itemBuilder: (BuildContext context, int i) {
            //       return Container(
            //         padding: EdgeInsets.only(bottom: 5),
            //         child: Column(
            //           children: <Widget>[
            //             Row(
            //               children: <Widget>[
            //                 Expanded(
            //                   flex: 1,
            //                   child: Text('${i + 1}.'),
            //                 ),
            //                 Expanded(
            //                   flex: 9,
            //                   child: Text('${dokterDetail['nama'][i]}'),
            //                 ),
            //               ],
            //             ),
            //             Row(
            //               children: <Widget>[
            //                 Expanded(
            //                   flex: 1,
            //                   child: Text(''),
            //                 ),
            //                 Expanded(
            //                   flex: 9,
            //                   child: Text('(${dokterDetail['keahlian'][i]})'),
            //                 ),
            //               ],
            //             ),
            //           ],
            //         ),
            //       );
            //     },
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}

class RowDataFaskes extends StatelessWidget {
  const RowDataFaskes({
    Key key,
    @required this.keyInfo,
    @required this.data,
  }) : super(key: key);

  final String keyInfo;
  final String data;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Expanded(child: Text(keyInfo)),
        Expanded(
          flex: 2,
          child: Text(': ' + data),
        )
      ],
    );
  }
}
